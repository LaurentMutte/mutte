---
title: "Ma page de recommandations"
order: 0
in_menu: true
---
Coucou voici un petit site avec des logiciels libres, pour arrêter avec google et son commerce de données, et pour essayer d'être en accord avec notre éthique.

Hola, aquí teniu un petit lloc amb programari lliure, per deixar amb Google i el seu comerç de dades, i per intentar estar d'acord amb la nostra ètica.

Retrouvez les détails sur [https://beta.framalibre.org/mini-site](https://beta.framalibre.org/mini-site)

Un exemple ci-dessous :

# googledocs



  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/CryptPad.png">
    </div>
    <div>
      <h2>CryptPad</h2>
      <p>CryptPad est une suite bureautique collaborative chifrée de bout en bout et open-source.</p>
      <div>
        <a href="https://framalibre.org/notices/cryptpad.html">Vers la notice Framalibre</a>
        <a href="https://cryptpad.org">Vers le site</a>
      </div>
    </div>
  </article>


## genre trello

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Kanboard.png">
    </div>
    <div>
      <h2>Kanboard</h2>
      <p>Kanboard, un logiciel libre pour gérer ses projets avec la méthode Kanban</p>
      <div>
        <a href="https://framalibre.org/notices/kanboard.html">Vers la notice Framalibre</a>
        <a href="https://kanboard.org">Vers le site</a>
       </div>
    </div>
  </article>

## treball collaboratiu

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Etherpad.png">
    </div>
    <div>
      <h2>Etherpad</h2>
      <p>Un éditeur de texte collaboratif et en temps réel !</p>
      <div>
        <a href="https://framalibre.org/notices/etherpad.html">Vers la notice Framalibre</a>
        <a href="http://etherpad.org/">Vers le site</a>
      </div>
    </div>
  </article>

## doodle

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Framadate.png">
    </div>
    <div>
      <h2>Framadate</h2>
      <p>Organiser des rendez-vous simplement, librement.</p>
      <div>
        <a href="https://framalibre.org/notices/framadate.html">Vers la notice Framalibre</a>
        <a href="https://framadate.org/">Vers le site</a>
      </div>
    </div>
  </article>

##  gestion socis

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Galette.png">
    </div>
    <div>
      <h2>Galette</h2>
      <p>Équipez votre association d'un outil de gestion des adhérents et des cotisations.</p>
      <div>
        <a href="https://framalibre.org/notices/galette.html">Vers la notice Framalibre</a>
        <a href="http://galette.eu">Vers le site</a>
      </div>
    </div>
  </article>

## gestio i valorizacio voluntaris

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/B%C3%A9n%C3%A9valibre.png">
    </div>
    <div>
      <h2>Bénévalibre</h2>
      <p>Un logiciel pour gérer et valoriser le bénévolat au sein de votre association.</p>
      <div>
        <a href="https://framalibre.org/notices/b%C3%A9n%C3%A9valibre.html">Vers la notice Framalibre</a>
        <a href="https://benevalibre.org/">Vers le site</a>
      </div>
    </div>
  </article>

## compta 

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Garradin.png">
    </div>
    <div>
      <h2>Garradin</h2>
      <p>Gestion d'association : membres, compta à double entrée, wiki, cotisations, site web, fichiers, etc.</p>
      <div>
        <a href="https://framalibre.org/notices/garradin.html">Vers la notice Framalibre</a>
        <a href="http://garradin.eu/">Vers le site</a>
      </div>
    </div>
  </article>

## compta casera


  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Paheko.png">
    </div>
    <div>
      <h2>Paheko</h2>
      <p>Gestion d'association : membres, compta pro mais simple, cotisations, site web, documents, etc.</p>
      <div>
        <a href="https://framalibre.org/notices/paheko.html">Vers la notice Framalibre</a>
        <a href="https://paheko.cloud/">Vers le site</a>
      </div>
    </div>
  </article> 

## cloud


  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Nextcloud.png">
    </div>
    <div>
      <h2>Nextcloud</h2>
      <p>Nextcloud est une plate-forme auto-hébergée de services de stockage et d’applications diverses dans les nuages.</p>
      <div>
        <a href="https://framalibre.org/notices/nextcloud.html">Vers la notice Framalibre</a>
        <a href="https://nextcloud.com/">Vers le site</a>
      </div>
    </div>
  </article> 